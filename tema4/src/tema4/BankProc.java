package tema4;

public interface BankProc {
	
	public void addAccount(Account account);
	public void deleteAccount(Account account);
	public void addPerson(Person person);
	public void deletePerson(Person person);
}
