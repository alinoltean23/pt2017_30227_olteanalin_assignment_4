package tema4;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;

public class Account {
	
	public int id;
	public int suma;
	Person person;

	private JFrame frame;
	private JTable table;
	private JTextField txtId;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnShowtable;
	private JTextField txtSuma;
	private JButton btnDeposit;
	private JButton btnWithdraw;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Account window = new Account();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
	public Account(){
		initialize();
	}
	
	public Account(int id, int suma, Person person) {
		this.id=id;
		this.suma=suma;
		this.person=person;
		
		
	}
	
	public int getSuma(){
		return suma; 
	}
	
	public int getId(){
		return id;
	}
	
	public void setSuma(int suma){
		this.suma=suma;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public void adaugaSuma(int suma){
		this.suma+=suma;
	}
	
	public void retrageSuma(int suma){
		this.suma-=suma;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1572, 1256);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAccount = new JLabel("Account");
		lblAccount.setHorizontalAlignment(SwingConstants.CENTER);
		lblAccount.setFont(new Font("Tahoma", Font.BOLD, 50));
		lblAccount.setBounds(0, 0, 434, 109);
		frame.getContentPane().add(lblAccount);
		

		JLabel lblId = new JLabel("Id");
		lblId.setBounds(26, 317, 161, 53);
		frame.getContentPane().add(lblId);
		
		table = new JTable();
		table.setBounds(611, 169, 823, 915);
		frame.getContentPane().add(table);
		
		txtId = new JTextField();
		txtId.setBounds(221, 324, 331, 39);
		frame.getContentPane().add(txtId);
		txtId.setColumns(10);
		
		JButton btnAddSaving = new JButton("Add Saving ");
		btnAddSaving.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnAddSaving.setBounds(65, 609, 197, 41);
		frame.getContentPane().add(btnAddSaving);
		
		btnEdit = new JButton("Edit");
		btnEdit.setBounds(213, 819, 171, 41);
		frame.getContentPane().add(btnEdit);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(213, 946, 171, 41);
		frame.getContentPane().add(btnDelete);
		
		btnShowtable = new JButton("ShowTable");
		btnShowtable.setBounds(1263, 1079, 171, 41);
		frame.getContentPane().add(btnShowtable);
		
		JButton btnAddSpending = new JButton("Add Spending");
		btnAddSpending.setBounds(331, 609, 197, 41);
		frame.getContentPane().add(btnAddSpending);
		
		JLabel lblSuma = new JLabel("Suma");
		lblSuma.setBounds(26, 426, 115, 33);
		frame.getContentPane().add(lblSuma);
		
		txtSuma = new JTextField();
		txtSuma.setColumns(10);
		txtSuma.setBounds(221, 423, 331, 39);
		frame.getContentPane().add(txtSuma);
		
		btnDeposit = new JButton("Deposit");
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeposit.setBounds(65, 710, 197, 41);
		frame.getContentPane().add(btnDeposit);
		
		btnWithdraw = new JButton("Withdraw");
		btnWithdraw.setBounds(331, 710, 197, 41);
		frame.getContentPane().add(btnWithdraw);
	}
}
