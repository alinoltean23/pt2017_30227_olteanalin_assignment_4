package tema4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Person {
	
	String nume;
	int cnp;

	private JFrame frame;
	private JTable table;
	private JTextField txtNume;
	private JTextField txtCnp;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnShowtable;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Person window = new Person();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Person() {
		initialize();
	}
	
	public Person(String nume, int cnp){
		this.nume=nume;
		this.cnp=cnp;
	}
	
	public String getNume(){
		return nume;
	}
	
	public int getCnp(){
		return cnp;
	}
	
	public void setNume(String nume){
		this.nume=nume;
	}
	
	public void setCnp(int cnp){
		this.cnp=cnp;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1572, 1256);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPerson = new JLabel("Person");
		lblPerson.setHorizontalAlignment(SwingConstants.CENTER);
		lblPerson.setFont(new Font("Tahoma", Font.BOLD, 50));
		lblPerson.setBounds(0, 0, 434, 109);
		frame.getContentPane().add(lblPerson);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(26, 213, 161, 53);
		frame.getContentPane().add(lblNume);
		
		table = new JTable();
		table.setBounds(611, 169, 823, 915);
		frame.getContentPane().add(table);
		
		txtNume = new JTextField();
		txtNume.setBounds(213, 220, 331, 39);
		frame.getContentPane().add(txtNume);
		txtNume.setColumns(10);
		
		JLabel lblCnp = new JLabel("CNP");
		lblCnp.setBounds(26, 333, 161, 53);
		frame.getContentPane().add(lblCnp);
		
		txtCnp = new JTextField();
		txtCnp.setColumns(10);
		txtCnp.setBounds(213, 340, 331, 39);
		frame.getContentPane().add(txtCnp);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnAdd.setBounds(211, 609, 171, 41);
		frame.getContentPane().add(btnAdd);
		
		btnEdit = new JButton("Edit");
		btnEdit.setBounds(213, 776, 171, 41);
		frame.getContentPane().add(btnEdit);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(213, 946, 171, 41);
		frame.getContentPane().add(btnDelete);
		
		btnShowtable = new JButton("ShowTable");
		btnShowtable.setBounds(1263, 1079, 171, 41);
		frame.getContentPane().add(btnShowtable);
	}
}
